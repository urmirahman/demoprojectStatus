<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate product object
include_once '../objects/project.php';
$database = new Database();
$db = $database->getConnection();
$project =new Project($db);
//get posted data
//make sure data is not empty
$data = json_decode(file_get_contents("php://input"));
if (
    !empty($data->name)&&
    !empty($data->project_manager)
){
    $project->name=$data->name;
    $project->project_manager=$data->project_manager;

    if($project->create()){
        http_response_code(201);
        echo json_encode(array("project created"));////
    }
    else{
        http_response_code(503);
        echo json_encode(array("unavailable to create project"));
    }
}

else{
    http_response_code(400);
    echo json_encode(array("Unable to create product. Data is incomplete"));
}
?>