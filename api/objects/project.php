<?php class Project{
// database connection and table name
    private $conn;
    private $table_name="projects";

    //properties

    public $id;
    public $name;
     public $project_manager;

     // constructor with $db as database connection
     public function __construct($db){
         $this->conn=$db;
     }

     function read(){
        $query = "SELECT   id , name, project_manager FROM `$this->table_name`";


         $statement = $this->conn->prepare($query);
         $statement->execute();
         return $statement;
     }

     function create(){
         $query= "INSERT into `$this->table_name` SET name=:name ,project_manager=:project_manager";

         $statement = $this->conn->prepare($query);


         $this->name=htmlspecialchars(strip_tags($this->name));
         $this->project_manager = htmlspecialchars(strip_tags($this->project_manager));

        $statement->bindParam(":name",$this->name);
        $statement->bindParam(":project_manager",$this->project_manager);


        if($statement->execute()){
            return true;
        }
        else{
            return false;
        }



     }

    
// update the product
function update(){
  
    // update query
    $query = "UPDATE `$this->table_name` SET name = :name, project_manager = :project_manager  WHERE id = :id";
  
    // prepare query statement
    $statement = $this->conn->prepare($query);
  
    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->project_manager=htmlspecialchars(strip_tags($this->project_manager));
    
    $this->id=htmlspecialchars(strip_tags($this->id));
  
    // bind new values
    $statement->bindParam(':name', $this->name);
    $statement->bindParam(':project_manager', $this->project_manager);
    
    $statement->bindParam(':id', $this->id);
  
    // execute the query
    if($statement->execute()){
        return true;
    }
  
    return false;
}

// delete the product
function delete(){
  
    // delete query
    $query = "DELETE FROM `$this->table_name` WHERE id = ?";
  
    // prepare query
    $stmt = $this->conn->prepare($query);
  
    // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
  
    // bind id of record to delete
    $stmt->bindParam(1, $this->id);
  
    // execute query
    if($stmt->execute()){
        return true;
    }
  
    return false;
}
}

?>